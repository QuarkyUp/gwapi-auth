package fr.gwapiauth.api;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HeartbeatController {

    @Value("${spring.application.name}")
    private String appName;

    @Value("${spring.application.version}")
    private String appVersion;

    @RequestMapping(value = "heartbeat", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Hearbeat heartbeat() {
        return new Hearbeat(appName, appVersion);
    }

    public class Hearbeat {
        public String name;
        public String version;

        public Hearbeat(String name, String version) {
            this.name = name;
            this.version = version;
        }
    }
}
