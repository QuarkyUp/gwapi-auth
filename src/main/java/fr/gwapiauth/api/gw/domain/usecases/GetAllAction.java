package fr.gwapiauth.api.gw.domain.usecases;

import fr.gwapiauth.api.gw.domain.ports.ActionRepositoryPort;
import fr.gwapiauth.api.gw.domain.ports.ClientRepositoryPort;
import fr.gwapiauth.api.gw.infra.entities.ActionEntity;
import fr.gwapiauth.api.gw.infra.entities.ClientEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetAllAction {
    private ActionRepositoryPort actionRepositoryPort;

    public GetAllAction(ActionRepositoryPort actionRepositoryPort) {
        this.actionRepositoryPort = actionRepositoryPort;
    }

    public List<ActionEntity> execute() {
        return actionRepositoryPort.getAll();
    }
}
