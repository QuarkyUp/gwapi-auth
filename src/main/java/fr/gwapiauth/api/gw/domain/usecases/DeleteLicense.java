package fr.gwapiauth.api.gw.domain.usecases;

import fr.gwapiauth.api.gw.domain.ports.LicenseRepositoryPort;
import org.springframework.stereotype.Service;

@Service
public class DeleteLicense {
    private LicenseRepositoryPort licenseRepositoryPort;

    public DeleteLicense(LicenseRepositoryPort licenseRepositoryPort) {
        this.licenseRepositoryPort = licenseRepositoryPort;
    }

    public void execute(String licenseKey) {
        licenseRepositoryPort.deleteLicense(licenseKey);
    }
}
