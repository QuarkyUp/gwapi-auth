package fr.gwapiauth.api.gw.domain.usecases;

import fr.gwapiauth.api.gw.domain.ports.ClientRepositoryPort;
import fr.gwapiauth.api.gw.infra.entities.ClientEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetAllClient {
    private ClientRepositoryPort clientRepositoryPort;

    public GetAllClient(ClientRepositoryPort clientRepositoryPort) {
        this.clientRepositoryPort = clientRepositoryPort;
    }

    public List<ClientEntity> execute() {
        return clientRepositoryPort.getAll();
    }
}
