package fr.gwapiauth.api.gw.domain.usecases;

import fr.gwapiauth.api.gw.domain.ports.LicenseRepositoryPort;
import fr.gwapiauth.api.gw.infra.entities.LicenseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class ValidateLicense {
    private LicenseRepositoryPort licenseRepositoryPort;

    public ValidateLicense(LicenseRepositoryPort licenseRepositoryPort) {
        this.licenseRepositoryPort = licenseRepositoryPort;
    }

    public Boolean execute(String key) {
        LicenseEntity licenseEntity = licenseRepositoryPort.getLicenseByKey(key);
        if (licenseEntity == null)
            return false;

        LocalDate now = LocalDate.now();
        boolean nowBetweenActivationAndExpiration =
                now.isAfter(licenseEntity.getActivationDate())
                && now.isBefore(licenseEntity.getExpirationDate());

        return nowBetweenActivationAndExpiration;
    }
}
