package fr.gwapiauth.api.gw.domain.usecases;

import fr.gwapiauth.api.gw.domain.ports.LicenseRepositoryPort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class UpdateLicense {
    private LicenseRepositoryPort licenseRepositoryPort;

    public UpdateLicense(LicenseRepositoryPort licenseRepositoryPort) {
        this.licenseRepositoryPort = licenseRepositoryPort;
    }

    public void execute(String licenseKeyToUpdate, Integer maxNumberOfAccounts, LocalDate activationDate, LocalDate expirationDate) {
        licenseRepositoryPort.updateLicense(licenseKeyToUpdate, maxNumberOfAccounts, activationDate, expirationDate);
    }
}
