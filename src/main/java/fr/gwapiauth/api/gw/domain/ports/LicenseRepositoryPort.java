package fr.gwapiauth.api.gw.domain.ports;

import fr.gwapiauth.api.gw.infra.entities.LicenseEntity;

import java.time.LocalDate;
import java.util.List;

public interface LicenseRepositoryPort {
    LicenseEntity createLicense(Integer maxNumberOfAccounts, String customerIdentifier, LocalDate activationDate, LocalDate expirationDate);

    LicenseEntity getLicenseByKey(String key);

    void deleteLicense(String licenseKey);

    void updateLicense(String licenseKeyToUpdate, Integer maxNumberOfAccounts, LocalDate activationDate, LocalDate expirationDate);

    List<LicenseEntity> getAllLicense();
}
