package fr.gwapiauth.api.gw.domain.usecases;

import fr.gwapiauth.api.gw.domain.ports.ActionRepositoryPort;
import fr.gwapiauth.api.gw.infra.entities.ActionEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetAllActionByClient {
    private ActionRepositoryPort actionRepositoryPort;

    public GetAllActionByClient(ActionRepositoryPort actionRepositoryPort) {
        this.actionRepositoryPort = actionRepositoryPort;
    }

    public List<ActionEntity> execute(Long clientId) {
        return actionRepositoryPort.getAllByClientId(clientId);
    }
}
