package fr.gwapiauth.api.gw.domain.ports;

import fr.gwapiauth.api.gw.infra.entities.ClientEntity;

import java.util.List;

public interface ClientRepositoryPort {
    List<ClientEntity> getAll();
}
