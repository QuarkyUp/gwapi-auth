package fr.gwapiauth.api.gw.domain.usecases;

import fr.gwapiauth.api.gw.domain.ports.LicenseRepositoryPort;
import fr.gwapiauth.api.gw.infra.entities.LicenseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class CreateLicense {
    private LicenseRepositoryPort licenseRepositoryPort;

    public CreateLicense(LicenseRepositoryPort licenseRepositoryPort) {
        this.licenseRepositoryPort = licenseRepositoryPort;
    }

    public LicenseEntity execute(Integer maxNumberOfAccounts, String customerIdentifier, LocalDate activationDate, LocalDate expirationDate) {
        return licenseRepositoryPort.createLicense(maxNumberOfAccounts, customerIdentifier, activationDate, expirationDate);
    }
}
