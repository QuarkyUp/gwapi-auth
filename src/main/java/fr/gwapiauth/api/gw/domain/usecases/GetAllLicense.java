package fr.gwapiauth.api.gw.domain.usecases;

import fr.gwapiauth.api.gw.domain.ports.LicenseRepositoryPort;
import fr.gwapiauth.api.gw.infra.entities.LicenseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GetAllLicense {
    private LicenseRepositoryPort licenseRepositoryPort;

    public GetAllLicense(LicenseRepositoryPort licenseRepositoryPort) {
        this.licenseRepositoryPort = licenseRepositoryPort;
    }

    public List<LicenseEntity> execute() {
        return licenseRepositoryPort.getAllLicense();
    }
}
