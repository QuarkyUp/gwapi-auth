package fr.gwapiauth.api.gw.domain.usecases;

import fr.gwapiauth.api.gw.domain.ports.ActionRepositoryPort;
import fr.gwapiauth.api.gw.infra.entities.ActionEntity;
import org.springframework.stereotype.Service;

@Service
public class CreateAction {
    private ActionRepositoryPort actionRepositoryPort;

    public CreateAction(ActionRepositoryPort actionRepositoryPort) {
        this.actionRepositoryPort = actionRepositoryPort;
    }

    public ActionEntity execute(Long clientId, String command) {
        return actionRepositoryPort.createAction(clientId, command);
    }
}
