package fr.gwapiauth.api.gw.domain.ports;

import fr.gwapiauth.api.gw.infra.entities.ActionEntity;

import java.util.List;

public interface ActionRepositoryPort {
    List<ActionEntity> getAll();
    List<ActionEntity> getAllByClientId(Long clientId);

    ActionEntity createAction(Long clientId, String command);
}
