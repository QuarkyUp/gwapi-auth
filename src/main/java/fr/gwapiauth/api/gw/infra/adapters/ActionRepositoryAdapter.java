package fr.gwapiauth.api.gw.infra.adapters;

import fr.gwapiauth.api.gw.domain.ports.ActionRepositoryPort;
import fr.gwapiauth.api.gw.infra.entities.ActionEntity;
import fr.gwapiauth.api.gw.infra.entities.ClientEntity;
import fr.gwapiauth.api.gw.infra.entities.repositories.ActionJpaRepository;
import fr.gwapiauth.api.gw.infra.entities.repositories.ClientJpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Repository
public class ActionRepositoryAdapter implements ActionRepositoryPort {
    private ActionJpaRepository actionJpaRepository;
    private ClientJpaRepository clientJpaRepository;

    public ActionRepositoryAdapter(ActionJpaRepository actionJpaRepository, ClientJpaRepository clientJpaRepository) {
        this.actionJpaRepository = actionJpaRepository;
        this.clientJpaRepository = clientJpaRepository;
    }

    @Override
    public List<ActionEntity> getAll() {
        return actionJpaRepository.findAll();
    }

    @Override
    public List<ActionEntity> getAllByClientId(Long clientId) {
        return actionJpaRepository.findAllByClientEntityId(clientId);
    }

    @Override
    public ActionEntity createAction(Long clientId, String command) {
        ClientEntity clientEntity = clientJpaRepository.findById(clientId)
                .orElseThrow(() -> new EntityNotFoundException("Client " + clientId + " does not exist"));

        return actionJpaRepository.save(ActionEntity.builder()
                .clientEntity(clientEntity)
                .command(command)
                .build());
    }
}
