package fr.gwapiauth.api.gw.infra.entities.repositories;

import fr.gwapiauth.api.gw.infra.entities.LicenseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LicenseJpaRepository extends JpaRepository<LicenseEntity, Long> {
    boolean existsByLicenseKey(String licenseKey);
    LicenseEntity findByLicenseKey(String licenseKey);
    void deleteByLicenseKey(String licenseKey);
}
