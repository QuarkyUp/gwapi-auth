package fr.gwapiauth.api.gw.infra.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity(name = "ACTION")
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class ActionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "ID")
    private Long id;

    @Column(name = "COMMAND")
    private String command;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "CLIENT_ID_FK", foreignKey = @ForeignKey(name = "FK_CLIENT_ACTION"))
    private ClientEntity clientEntity;
}
