package fr.gwapiauth.api.gw.infra.adapters;

import fr.gwapiauth.api.gw.domain.ports.LicenseRepositoryPort;
import fr.gwapiauth.api.gw.infra.entities.LicenseEntity;
import fr.gwapiauth.api.gw.infra.entities.repositories.LicenseJpaRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.List;

@Repository
public class LicenseRepositoryAdapter implements LicenseRepositoryPort {
    private LicenseJpaRepository licenseJpaRepository;

    public LicenseRepositoryAdapter(LicenseJpaRepository licenseJpaRepository) {
        this.licenseJpaRepository = licenseJpaRepository;
    }

    @Override
    public LicenseEntity createLicense(Integer maxNumberOfAccounts, String customerIdentifier, LocalDate activationDate, LocalDate expirationDate) {
        int licenseKeyLength = 20;
        String licenseKey = RandomStringUtils.randomAlphanumeric(licenseKeyLength);
        boolean existingLicense = licenseJpaRepository.existsByLicenseKey(licenseKey);

        while (existingLicense) {
            existingLicense = licenseJpaRepository.existsByLicenseKey(licenseKey);
            licenseKey = RandomStringUtils.randomAlphanumeric(licenseKeyLength);
        }

        return licenseJpaRepository.save(LicenseEntity.builder()
                .maxNumberOfAccounts(maxNumberOfAccounts)
                .customerIdentifier(customerIdentifier)
                .activationDate(activationDate)
                .expirationDate(expirationDate)
                .licenseKey(licenseKey)
                .build());
    }

    @Override
    public LicenseEntity getLicenseByKey(String key) {
        return licenseJpaRepository.findByLicenseKey(key);
    }

    @Override
    public void deleteLicense(String licenseKey) {
        licenseJpaRepository.deleteByLicenseKey(licenseKey);
    }

    @Override
    public void updateLicense(String licenseKeyToUpdate, Integer maxNumberOfAccounts, LocalDate activationDate, LocalDate expirationDate) {
        LicenseEntity licenseEntityToUpdate = licenseJpaRepository.findByLicenseKey(licenseKeyToUpdate);
        if (licenseEntityToUpdate == null)
            throw new EntityNotFoundException("License " + licenseKeyToUpdate + " does not exist");

        LicenseEntity updatedLicenseEntity = licenseEntityToUpdate.toBuilder()
                .maxNumberOfAccounts(maxNumberOfAccounts)
                .activationDate(activationDate)
                .expirationDate(expirationDate)
                .build();

        licenseJpaRepository.save(updatedLicenseEntity);
    }

    @Override
    public List<LicenseEntity> getAllLicense() {
        return licenseJpaRepository.findAll();
    }
}
