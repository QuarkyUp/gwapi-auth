package fr.gwapiauth.api.gw.infra.entities.repositories;

import fr.gwapiauth.api.gw.infra.entities.ActionEntity;
import fr.gwapiauth.api.gw.infra.entities.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActionJpaRepository extends JpaRepository<ActionEntity, Long> {
    List<ActionEntity> findAll();
    List<ActionEntity> findAllByClientEntityId(Long clientId);
}
