package fr.gwapiauth.api.gw.infra.entities.repositories;

import fr.gwapiauth.api.gw.infra.entities.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientJpaRepository extends JpaRepository<ClientEntity, Long> {
    List<ClientEntity> findAll();
}
