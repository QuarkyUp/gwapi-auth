package fr.gwapiauth.api.gw.infra.adapters;

import fr.gwapiauth.api.gw.domain.ports.ClientRepositoryPort;
import fr.gwapiauth.api.gw.infra.entities.ClientEntity;
import fr.gwapiauth.api.gw.infra.entities.repositories.ClientJpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ClientRepositoryAdapter implements ClientRepositoryPort {
    private ClientJpaRepository clientJpaRepository;

    public ClientRepositoryAdapter(ClientJpaRepository clientJpaRepository) {
        this.clientJpaRepository = clientJpaRepository;
    }

    @Override
    public List<ClientEntity> getAll() {
        return clientJpaRepository.findAll();

    }
}
