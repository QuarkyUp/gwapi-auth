package fr.gwapiauth.api.gw.infra.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;

@Entity(name = "LICENSE")
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class LicenseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "ID")
    private Long id;

    @Column(name = "MAX_ACCOUNT_COUNT")
    private Integer maxNumberOfAccounts;

    @Column(name = "CUSTOMER_IDENTIFIER")
    private String customerIdentifier;

    @Column(name = "ACTIVATION_DATE")
    private LocalDate activationDate;

    @Column(name = "EXPIRATION_DATE")
    private LocalDate expirationDate;

    @Column(name = "LICENSE_KEY")
    private String licenseKey;
}
