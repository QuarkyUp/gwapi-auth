package fr.gwapiauth.api.gw.application.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class LicenseUpdateDtoInput {
    @NotNull
    private Integer maxNumberOfAccounts;
    @NotNull
    private LocalDate activationDate;
    @NotNull
    private LocalDate expirationDate;
}
