package fr.gwapiauth.api.gw.application.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Builder
@Getter
@AllArgsConstructor
public class ActionDtoInput {
    @NotNull
    private String command;
    @NotNull
    private Long clientId;
}
