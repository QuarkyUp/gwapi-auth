package fr.gwapiauth.api.gw.application.controllers;

import fr.gwapiauth.api.gw.application.dtos.ClientDtoOutput;
import fr.gwapiauth.api.gw.domain.usecases.GetAllClient;
import fr.gwapiauth.api.gw.infra.entities.ClientEntity;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Api(value = "Clients", description = "Client endpoint")
@RestController
@RequestMapping("/gwapi/v1")
public class ClientController {
    private final GetAllClient getAllClient;

    public ClientController(GetAllClient getAllClient) {
        this.getAllClient = getAllClient;
    }

    @GetMapping("/clients")
    public ResponseEntity<List<ClientDtoOutput>> list() {
        List<ClientEntity> clients = getAllClient.execute();

        List<ClientDtoOutput> clientDtos = clients.stream()
                .map(ClientDtoOutput::new)
                .collect(toList());

        return ResponseEntity.ok(clientDtos);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity genericExceptionHandler(HttpServletRequest request,
                                                  EntityNotFoundException exception) {
        return new ResponseEntity<>(BAD_REQUEST);
    }
}
