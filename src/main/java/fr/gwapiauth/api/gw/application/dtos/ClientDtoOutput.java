package fr.gwapiauth.api.gw.application.dtos;

import fr.gwapiauth.api.gw.infra.entities.ClientEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;

@Builder
@Getter
@AllArgsConstructor
public class ClientDtoOutput {
    private Long clientId;
    private Long processId;
    private LocalDate injectionDate;

    public ClientDtoOutput(ClientEntity clientEntity) {
        clientId = clientEntity.getId();
        processId = clientEntity.getProcessId();
        injectionDate = clientEntity.getInjectionDate();
    }
}
