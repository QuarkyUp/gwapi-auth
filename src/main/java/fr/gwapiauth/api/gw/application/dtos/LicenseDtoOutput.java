package fr.gwapiauth.api.gw.application.dtos;

import fr.gwapiauth.api.gw.infra.entities.LicenseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;

@Builder
@Getter
@AllArgsConstructor
public class LicenseDtoOutput {
    private Long licenseId;
    private Integer maxAccountCount;
    private String customerIdentifier;
    private String licenseKey;
    private LocalDate activationDate;
    private LocalDate expirationDate;

    public LicenseDtoOutput(LicenseEntity licenseEntity) {
        licenseId = licenseEntity.getId();
        maxAccountCount = licenseEntity.getMaxNumberOfAccounts();
        customerIdentifier = licenseEntity.getCustomerIdentifier();
        licenseKey = licenseEntity.getLicenseKey();
        activationDate = licenseEntity.getActivationDate();
        expirationDate = licenseEntity.getExpirationDate();
    }
}
