package fr.gwapiauth.api.gw.application.dtos;

import fr.gwapiauth.api.gw.infra.entities.ActionEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
@AllArgsConstructor
public class ActionDtoOutput {
    private Long actionId;
    private String command;
    private Long clientId;

    public ActionDtoOutput(ActionEntity actionEntity) {
        clientId = actionEntity.getId();
        command = actionEntity.getCommand();
        clientId = actionEntity.getClientEntity().getId();
    }
}
