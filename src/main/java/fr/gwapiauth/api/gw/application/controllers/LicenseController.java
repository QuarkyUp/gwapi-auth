package fr.gwapiauth.api.gw.application.controllers;

import fr.gwapiauth.api.gw.application.dtos.LicenseDtoInput;
import fr.gwapiauth.api.gw.application.dtos.LicenseDtoOutput;
import fr.gwapiauth.api.gw.application.dtos.LicenseUpdateDtoInput;
import fr.gwapiauth.api.gw.application.dtos.LicenseValidationDtoInput;
import fr.gwapiauth.api.gw.domain.usecases.*;
import fr.gwapiauth.api.gw.infra.entities.LicenseEntity;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;

@Api(value = "Licenses", description = "License endpoint")
@RestController
@RequestMapping("/gwapi/v1")
public class LicenseController {
    private final ValidateLicense validateLicense;
    private final CreateLicense createdLicense;
    private final DeleteLicense deleteLicense;
    private final UpdateLicense updateLicense;
    private final GetAllLicense getAllLicense;

    public LicenseController(ValidateLicense validateLicense, CreateLicense createdLicense, DeleteLicense deleteLicense, UpdateLicense updateLicense, GetAllLicense getAllLicense) {
        this.validateLicense = validateLicense;
        this.createdLicense = createdLicense;
        this.deleteLicense = deleteLicense;
        this.updateLicense = updateLicense;
        this.getAllLicense = getAllLicense;
    }

    @GetMapping("/licenses")
    public ResponseEntity<List<LicenseDtoOutput>> list() {
        List<LicenseEntity> actions = getAllLicense.execute();

        List<LicenseDtoOutput> licenseDtos = actions.stream()
                .map(LicenseDtoOutput::new)
                .collect(toList());

        return ResponseEntity.ok(licenseDtos);
    }

    @PostMapping("/license/validate")
    public ResponseEntity validateLicense(@RequestBody @Valid LicenseValidationDtoInput licenseValidationDtoInput) {
        Boolean isLicenseValid = validateLicense.execute(licenseValidationDtoInput.getLicenseKey());

        if (isLicenseValid)
            return new ResponseEntity(HttpStatus.OK);
        else
            return new ResponseEntity(HttpStatus.FORBIDDEN);
    }

    @PostMapping("/licenses")
    public ResponseEntity createLicense(@RequestBody @Valid LicenseDtoInput licenseDtoInput) {
        LicenseEntity licenseEntity = createdLicense.execute(licenseDtoInput.getMaxNumberOfAccounts(),
                licenseDtoInput.getCustomerIdentifier(),
                licenseDtoInput.getActivationDate(),
                licenseDtoInput.getExpirationDate());

        return new ResponseEntity<>(licenseEntity.getLicenseKey(), CREATED);
    }

    @PutMapping("/licenses/{licenseKeyToUpdate}")
    public ResponseEntity update(@PathVariable String licenseKeyToUpdate, @RequestBody @Valid LicenseUpdateDtoInput licenseUpdateDtoInput) {
        updateLicense.execute(licenseKeyToUpdate,
                licenseUpdateDtoInput.getMaxNumberOfAccounts(),
                licenseUpdateDtoInput.getActivationDate(),
                licenseUpdateDtoInput.getExpirationDate());
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/licenses/{licenseKey}")
    public ResponseEntity<Void> delete(@PathVariable String licenseKey) {
        deleteLicense.execute(licenseKey);
        return ResponseEntity.noContent().build();
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity genericExceptionHandler(HttpServletRequest request,
                                                  EntityNotFoundException exception) {
        return new ResponseEntity<>(BAD_REQUEST);
    }
}
