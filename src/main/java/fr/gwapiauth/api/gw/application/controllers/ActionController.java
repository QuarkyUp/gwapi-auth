package fr.gwapiauth.api.gw.application.controllers;

import fr.gwapiauth.api.gw.application.dtos.ActionDtoInput;
import fr.gwapiauth.api.gw.application.dtos.ActionDtoOutput;
import fr.gwapiauth.api.gw.domain.usecases.CreateAction;
import fr.gwapiauth.api.gw.domain.usecases.GetAllAction;
import fr.gwapiauth.api.gw.domain.usecases.GetAllActionByClient;
import fr.gwapiauth.api.gw.infra.entities.ActionEntity;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;

@Api(value = "Actions", description = "Action endpoint")
@RestController
@RequestMapping("/gwapi/v1")
public class ActionController {
    private final GetAllAction getAllAction;
    private final GetAllActionByClient getAllActionByClient;
    private final CreateAction createAction;

    public ActionController(GetAllAction getAllAction, GetAllActionByClient getAllActionByClient, CreateAction createAction) {
        this.getAllAction = getAllAction;
        this.getAllActionByClient = getAllActionByClient;
        this.createAction = createAction;
    }

    @GetMapping("/actions")
    public ResponseEntity<List<ActionDtoOutput>> list() {
        List<ActionEntity> actions = getAllAction.execute();

        List<ActionDtoOutput> clientDtos = actions.stream()
                .map(ActionDtoOutput::new)
                .collect(toList());

        return ResponseEntity.ok(clientDtos);
    }

    @GetMapping("/client/{clientId}/actions")
    public ResponseEntity<List<ActionDtoOutput>> listByClient(@PathVariable Long clientId) {
        List<ActionEntity> actions = getAllActionByClient.execute(clientId);

        List<ActionDtoOutput> clientDtos = actions.stream()
                .map(ActionDtoOutput::new)
                .collect(toList());

        return ResponseEntity.ok(clientDtos);
    }

    @PostMapping("/actions")
    public ResponseEntity create(@RequestBody @Valid ActionDtoInput actionDtoInput) {
        ActionEntity actionEntity = createAction.execute(actionDtoInput.getClientId(),
                actionDtoInput.getCommand());

        return new ResponseEntity<>(actionEntity.getId(), CREATED);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity genericExceptionHandler(HttpServletRequest request,
                                                  EntityNotFoundException exception) {
        return new ResponseEntity<>(BAD_REQUEST);
    }
}
